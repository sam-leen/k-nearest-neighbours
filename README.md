# k-nearest-neighbours
an implementation of the KNN algorithm on R^3 space, using XML to store data

The K nearest neighbours uses the properties of nodes in a metric space to predict properties of nodes as defined by the user.
